#jump_servers/outputs

output "iam" {
  value = module.iam
}

output "ec2" {
  value = module.ec2
}

output "security_groups" {
  value = module.security_groups
}
