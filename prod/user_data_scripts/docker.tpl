
#!/bin/bash -xe
echo 'begin install ======================================================================='
until ping -c1 www.google.com &>/dev/null; do
    echo "Waiting for network ..."
    sleep 1
done
yum update -y
yum install -y jq
yum install -y wget
yum install -y docker
service docker start
systemctl enable --now docker
usermod -a -G docker ec2-user
curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
echo TOKEN=$(aws rds generate-db-auth-token --hostname mosar-db.clomwr36ba7e.eu-central-1.rds.amazonaws.com --port 3306 --username x0N8H3KViXFgl43ts --region eu-central-1) | tee -a /home/bla
