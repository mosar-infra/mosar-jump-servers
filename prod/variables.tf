# ws jump_servers/variables.tf

variable "environment" {
  default = "prod"
}
variable "managed_by" {
  default = "jump_servers"
}
variable "access_ip" {}
