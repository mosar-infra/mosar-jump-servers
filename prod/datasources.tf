# ws jump servers/datasource

data "aws_vpc" "vpc" {
  dynamic "filter" {
    for_each = local.env_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnets" "private" {
  dynamic "filter" {
    for_each = local.private_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "private" {
  for_each = toset(data.aws_subnets.private.ids)
  id       = each.value
}

data "aws_subnets" "public" {
  dynamic "filter" {
    for_each = local.public_subnet_filter
    content {
      name   = filter.value.name
      values = filter.value.values
    }
  }
}

data "aws_subnet" "public" {
  for_each = toset(data.aws_subnets.public.ids)
  id       = each.value
}

data "aws_key_pair" "mosar_server_ssh_prod" {
  key_name = "mosar_server_ssh_${var.environment}"
  filter {
    name   = "tag:Environment"
    values = ["${var.environment}"]
  }
}

data "aws_security_group" "nat_instance" {
  name = "nat_instance_sg"
  tags = {
    Environment = var.environment
  }
}

