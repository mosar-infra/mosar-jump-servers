# ws jump_servers /main.tf

module "iam" {
  source      = "git::https://gitlab.com/mosar-infra/tf-module-iam.git?ref=tags/v3.0.2"
  roles       = local.roles
  profiles    = local.profiles
  environment = "test"
}

module "ec2" {
  source                  = "git::https://gitlab.com/mosar-infra/tf-module-ec2.git?ref=tags/v1.0.3"
  ami_name_string         = "amzn2-ami-hvm-2.0*"
  ami_owners              = ["amazon"]
  nodes                   = local.nodes
  environment             = var.environment
  managed_by              = var.managed_by
}

module "security_groups" {
  source                   = "git::https://gitlab.com/mosar-infra/tf-module-security-groups.git?ref=tags/v1.2.2"
  environment              = var.environment
  managed_by               = var.managed_by
  security_groups          = local.security_groups
}

