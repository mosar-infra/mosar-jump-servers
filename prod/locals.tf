#  ws jump servers/locals.tf

locals {
  security_groups = {
    bastion-private = {
      vpc_id       = data.aws_vpc.vpc.id
      name         = "bastion_private_sg"
      description  = "security group for bastion/ssh access"
      ingress_cidr = {}
      ingress_sg = {
        ssh = {
          from            = 22
          to              = 22
          protocol        = "tcp"
          security_groups = [data.aws_security_group.nat_instance.id]
        }
      }
      egress = {
        http = {
          from        = 0
          to          = 0
          protocol    = -1
          cidr_blocks = ["0.0.0.0/0"]
        }
      }
    }
  }
}

locals {
  roles = {
    admin_frankfurt = {
      role = {
        name               = "admin_frankfurt_${var.environment}"
        description        = "admin role for region eu central 1"
        assume_role_policy = file("${path.root}/iam_spec_files/assume_role_policy_ec2.json")
      }
      policy = {
        name        = "admin_frankfurt_policy_${var.environment}"
        description = "Policy to all resources and actions in frankfurt"
        policy      = file("${path.root}/iam_spec_files/policy_admin_frankfurt.json")
      }
      policy_attachment = {
        name = "admin-frankfurt-policy-attachment"
      }
    }
  }
}

locals {
  profiles = {
    admin_frankfurt = {
      name = "admin_frankfurt_profile_${var.environment}"
    }
  }
}

locals {
  subnets = {
    private = [for s in data.aws_subnet.private : s.id]
    public  = [for s in data.aws_subnet.public : s.id]

  }
}

locals {
  nodes = {
    bastion-private = {
      instance_type               = "t2.micro"
      subnet_ids                  = local.subnets.private
      vpc_security_group_ids      = module.security_groups.security_groups["bastion-private"].*.id
      associate_public_ip_address = false
      root_block_device_vol_size  = 10
      iam_instance_profile        = module.iam.profile["admin_frankfurt"].name
      tags_name_prefix            = "bastion_server_private"
      user_data_file_path         = "./user_data_scripts/docker.tpl"
      script_vars                 = {}
      private_ip                  = local.private_ips["bastion-private"]
      key_name                    = data.aws_key_pair.mosar_server_ssh_prod.key_name
    }
  }
}

locals {
  private_ips = {
    bastion-private = split("/", cidrsubnet([for s in data.aws_subnet.private : s.cidr_block][0], 8, 254))[0]
  }
}

locals {
  private_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*private*"]
  }]
}
locals {
  public_subnet_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
    },
    {
      name   = "tag:Name"
      values = ["*public*"]
  }]
}

locals {
  env_filter = [{
    name   = "tag:Environment"
    values = [var.environment]
  }]
}
