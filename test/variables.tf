# ws jump_servers/variables.tf

variable "environment" {
  default = "test"
}
variable "managed_by" {
  default = "jump_servers"
}
variable "access_ip" {}
